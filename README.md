# Local setup

Local setup assumes you have Python 3.+ and VEnv installed and are comfortable working with both.

These rough instructions are provided specifically for a Windows enviroment and 
a Powershell cmd line.

1. Clone the repo into a new directory
2. Move into that directory `cd mapillary_uploader`
3. Create a virtual enviroment `python -m venv venv`
4. Enter the the virtual enviroment `venv/Scripts/Activate.ps1`
5. Install python requirements `pip install -r requirements.txt`
6. Install the mapillary tools `pip install --upgrade git+https://github.com/mapillary/mapillary_tools@v0.8.0`
7. Replace the bogus info in `example.env` with your credentials and save the file as `.env`
8. Test with `python mapillary_uploader.py test/safety.csv --verbose --dry_run`

To test the go_pro_uploader script use:
`python .\go_pro_uploader.py .\test\go_pro_test\DCIM\ --verbose --dry_run`

These commands should work on Ubuntu (mileage may vary).

1. Clone the repo into a new directory
2. Move into that directory `cd mapillary_uploader`
3. Create a virtual enviroment `python -m venv venv`
4. Enter the the virtual enviroment `source venv/bin/activate`
5. Install python requirements `pip install -r requirements.txt`
6. Install the mapillary tools `pip install --upgrade git+https://github.com/mapillary/mapillary_tools@v0.8.0`
7. Replace the bogus info in `example.env` with your credentials and save the file as `.env`
8. Test with `python mapillary_uploader.py test/safety.csv --verbose --dry_run`



# Further reading

[Mapillary Tools Docs](https://github.com/mapillary/mapillary_tools/)