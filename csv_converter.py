import csv
import posixpath
import dateutil.parser
import os.path

def mapillary_test(csv_path):
    delimiter=","
    header=False
    csv_data = None
    with open(csv_path, "r") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=delimiter)
        print(csvreader)
        if header:
            next(csvreader, None)
        csv_data = zip(*csvreader)
    print(list(csv_data))
    input()

    filename_column = 0

    # align by filename column if provided, otherwise align in order of image
    # names
    file_names = None
    if filename_column:
            file_names = csv_data[filename_column - 1]
    print("filenames: ", file_names)
    input()


# Convert a Safety CSV file to a Mapillary CSV file.
def convert(csv_input_path, csv_output_path, verbose=False):
    input_file = open(csv_input_path, newline='')
    csv_reader = csv.DictReader(input_file)
    output_file = open(csv_output_path, 'w', newline='')
    csv_writer = csv.DictWriter(f = output_file,fieldnames=['filename', 'timestamp', 'latitude', 'longitude', 'heading'])
    file_names = {}
    file_names['passenger_front'] = []
    file_names['driver_front'] = []
    file_names['passenger_low'] = []
    survey_name = ''
    if verbose:
        print('\nConverting CSV:')
        i = 10 # to print the first i rows
    for row in csv_reader:
        filename = os.path.abspath(os.path.join('data/images/', row['file'].split('/')[-1]))
        timestamp = dateutil.parser.isoparse(row['time']).strftime('%Y:%m:%d %H:%M:%S.%f')
        # convert the camera position into a heading in degrees
        if row['position'] == 'pf':
            heading = 25
        elif row['position'] == 'df':
            heading = 345
        elif row['position'] == 'pl':
            heading = 90
        else:
            heading = 0
        mapillary_row = {
            'filename': filename,
            'timestamp': timestamp,
            'latitude': row['lat'],
            'longitude': row['lon'],
            'heading': heading
        }
        # Write the row to the output file.
        csv_writer.writerow(mapillary_row)
        # Add the image name to the list of images based on the camera
        if row['position'] == 'pf':
            file_names['passenger_front'].append(row['file'])
        elif row['position'] == 'df':
            file_names['driver_front'].append(row['file'])
        elif row['position'] == 'pl':
            file_names['passenger_low'].append(row['file']) 
        if verbose:
            if i > 0:
                print(mapillary_row)
                i = i - 1
    # Get the survey name from the last row
    survey_name = row['survey']
    # Flush and close the output file.
    output_file.flush()
    output_file.close()

    return file_names, survey_name
