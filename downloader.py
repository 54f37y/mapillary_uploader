import boto3
import os
import itertools
from decouple import config

boto_session = boto3.Session(
    aws_access_key_id=config('aws_access_key_id'),
    aws_secret_access_key=config('aws_secret_access_key'),
)
s3_client = boto_session.client('s3')


# Download all files in an AWS bucket from a path prefix.
# Optionally restrict the download to a list of filenames.
def download_s3_dir(bucket, prefix, output_dir, filenames=None, verbose=False):
    base_kwargs = {
        'Bucket': bucket,
        'Prefix': prefix,
    }
    keys = []
    next_token = ''
    if verbose:
        print("\nDownloading images:")
    # Walk the objects in the bucket and extract their keys.
    while next_token is not None:
        kwargs = base_kwargs.copy()
        if next_token != '':
            kwargs.update({'ContinuationToken': next_token})
        results = s3_client.list_objects_v2(**kwargs)
        contents = results.get('Contents')
        for i in contents:
            if verbose:
                print('Searching for files [%s/%s]' % (len(keys), len(filenames)), 
                      end='\r')
            k = i.get('Key')
            if k[-1] != '/':
                # Only extract keys of matching filenames.
                f = k.split('/')[-1]
                if filenames is None or f in filenames:
                    keys.append(k)

        next_token = results.get('NextContinuationToken')
    # Download the objects.
    if verbose:
        print('')
        count = 1
    for k in keys:
        if verbose:
            print("Downloading files [%s/%s]" % (count, len(keys)), end="\r")
        # use just the file name not the whole path 
        dest_path = os.path.join(output_dir, k.split('/')[-1])
        if not os.path.exists(os.path.dirname(dest_path)):
            os.makedirs(os.path.dirname(dest_path))
        s3_client.download_file(bucket, k, dest_path)
        count = count + 1
    print('')    
