import argparse
import os
import pandas as pd
import piexif
from datetime import datetime as dt
from datetime import timedelta as td
import geopy.distance as gd

def main():
    # Setup argparse
    parser = argparse.ArgumentParser(description="Uploads all images on a Go \
        Pro formatted SD card to Mapillary.")
    parser.add_argument('-p','--path', 
                        action='append', 
                        help='Path(s) to the DCIM folder(s) of a Go Pro. \
                            Specify multiple with "-p <path1> -p <path2>"', 
                        required=True)
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    args = parser.parse_args()
    if args.verbose:
        print("args: ", args)
    for p in args.path:
        if args.verbose:
            print("Checking: {}".format(p))
        check(p, args.verbose)

def check(goPro_DCIM_path, verbose):
    '''
    This script runs over a GoPro DCIM folder and identifies:
    - Path, file name, capture time, latitude and longitude, and estimated speed of each file
    - It writes out a CSV as a check artifact in the DCIM folder 
    '''
    # check the inputs
    if not os.path.exists(goPro_DCIM_path):
        print("Error: Given Go Pro path does not exist")
        return
    # create a data frame for all the details
    df = pd.DataFrame(columns=['file_name', 'parent_dir', 'path', 'lat', 'lon', 'datetime', 'is_jpg'])
    # work through each folder of images
    for dir in os.listdir(goPro_DCIM_path):
        # we identify the image folders by the presence of GOPRO in their names
        image_path = os.path.join(goPro_DCIM_path, dir)
        if os.path.isdir(image_path) and dir.find("GOPRO") != -1:
            for file in os.listdir(image_path):
                file_path = os.path.join(image_path, file)
                # make sure the file is a jpg
                if file.find(".JPG") == -1:
                    data = {'file_name':file, 
                            'parent_dir':dir, 
                            'path':file_path, 
                            'is_jpg':False}
                else:
                    # these should stay the same between go pro versions, but no promises!
                    date_tag = 'DateTime'
                    lat_tag = 'GPSLatitude'
                    lat_dir_tag = 'GPSLatitudeRef'
                    lon_tag = 'GPSLongitude'
                    lon_dir_tag = 'GPSLongitudeRef'
                    exif_dict = piexif.load(file_path)
                    # set these to None in case they don't exist in the Exif
                    date_str = date_dt = lat_tpl = lon_tpl = lat_dir = lon_dir = None
                    for ifd in ("0th", "Exif", "GPS", "1st"):
                        for tag in exif_dict[ifd]:
                            # general print statement for checking Exif structure -- comment out for production!
                            # print(piexif.TAGS[ifd][tag]["name"], exif_dict[ifd][tag])
                            # pull the creation date & time for each image
                            if piexif.TAGS[ifd][tag]["name"] == date_tag:
                                date_str = exif_dict[ifd][tag].decode("utf-8")
                                date_dt = dt.strptime(date_str, '%Y:%m:%d %H:%M:%S')
                            # pull the GPS coordinates for each image
                            if piexif.TAGS[ifd][tag]["name"] == lat_tag:
                                lat_tpl = exif_dict[ifd][tag]
                            if piexif.TAGS[ifd][tag]["name"] == lon_tag:
                                lon_tpl = exif_dict[ifd][tag]
                            if piexif.TAGS[ifd][tag]["name"] == lat_dir_tag:
                                lat_dir = exif_dict[ifd][tag].decode("utf-8")
                            if piexif.TAGS[ifd][tag]["name"] == lon_dir_tag:
                                lon_dir = exif_dict[ifd][tag].decode("utf-8")
                    # convert the gps coordinates into decimal form
                    if lat_tpl is not None and lon_tpl is not None and lat_dir is not None and lon_dir is not None:
                        lat = dms_to_deg(degrees=lat_tpl[0][0],
                                        minutes=lat_tpl[1][0],
                                        seconds=(lat_tpl[2][0]/lat_tpl[2][1]),
                                        direction=lat_dir)
                        lon = dms_to_deg(degrees=lon_tpl[0][0],
                                        minutes=lon_tpl[1][0],
                                        seconds=(lon_tpl[2][0]/lon_tpl[2][1]),
                                        direction=lon_dir)
                    else:
                        lat = lon = None
                    data = {'file_name':file, 
                            'parent_dir':dir, 
                            'path':file_path, 
                            'lat':lat, 
                            'lon':lon, 
                            'datetime':date_dt, 
                            'is_jpg':True}
                # test statement to only look at the first file in each folder - comment out for production!
                # print(data)
                # break
                # add the new row to the dataframe
                df = df.append(data, ignore_index=True)
        if verbose:
            print("Completed: {}".format(image_path))
        # test statement to only look at the first folder - comment out for production!
        # break
    # increment the date time based on the index, since GoPro doesn't give us decimal seconds
    df['datetime'] = pd.to_datetime(df['datetime'])
    df = df.sort_values('datetime', ignore_index=True)
    df['id'] = df.index.astype(int)
    df['dt_increment'] = pd.to_timedelta((((df['id'] + 1) % 2)/2)*1000, unit='milliseconds')
    df['datetime'] = df['datetime'] + df['dt_increment']
    # collect the location of the previous frame
    df['prev_lat'] = df['lat'].shift(1)
    df['prev_lon'] = df['lon'].shift(1)
    df['prev_dt'] = df['datetime'].shift(1)
    df = df.dropna(axis=0) # drops the first image since it has no prev
    # calc the distance
    df['distance_miles'] = df.apply(lambda x: gd.distance((x['prev_lat'], x['prev_lon']),(x['lat'], x['lon'])).miles, axis='columns')
    # calculate the elapsed time
    df['elapsed_dt'] = df['datetime'] - df['prev_dt']
    df['elapsed_hours'] = (df['elapsed_dt'] / pd.Timedelta(seconds=1))/3600
    # calculated the percieved speed
    df['speed_mph'] = df['distance_miles'] / df['elapsed_hours']
    # save the dataframe as a CSV in the given folder
    today = dt.strftime(dt.today(),'%Y_%m_%d-%H_%M_%S')
    csv_path = os.path.join(goPro_DCIM_path, 'safe7y_check-' + today + '.csv')
    with open(csv_path, 'w') as outfile:
        df.to_csv(outfile, line_terminator='\n')
    # print data frame statistics
    # if verbose:
    #     print(df)
    #     print(df.dtypes)
    # TODO: map the data frame automatically

def dms_to_deg(degrees, minutes, seconds, direction):
# converts a lat or lon expressed in degrees, minutes, seconds and 
# cardinal direction ("N", "S", "E", "W") to a decimal
    dd = float(degrees) + float(minutes)/60 + float(seconds)/(60*60)
    if direction == 'S' or direction == 'W':
        dd *= -1
    return dd

if __name__ == '__main__':
    main()
