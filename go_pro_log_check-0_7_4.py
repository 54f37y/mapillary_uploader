import os
import argparse
import pandas as pd
from datetime import datetime as dt

def main():
    # Setup argparse
    parser = argparse.ArgumentParser(description="Checks the generated log file and print statistics.")
    parser.add_argument("go_pro_log_path",
                        help="Path to a go_pro_uploader.py log file")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    args = parser.parse_args()
    if args.verbose:
        print("args: ", args)
    # check the inputs
    path = args.go_pro_log_path
    if not os.path.exists(path) or  path.find(".log") == -1:
        print("Error: Given Go Pro log file does not exist")
        return
    # create a data frame for all the details
    df = pd.DataFrame(columns=['dir', 'path', 'warnings', 'uploading', 'skipped', 'uploaded'])
    data = None
    # work through each file
    if args.verbose:
        print('Checking: {}'.format(path))
    with open(path, 'r', encoding='UTF-8') as f:
        for line in f:
            # Check to see if we have started work on a new file 
            if "Uploading: ['mapillary_tools', 'process_and_upload', '--import_path'," in line:
                # save the statistics for the last file if they exists
                if data != None:
                    data = {'dir': os.path.split(file_path)[-1], 'path': file_path, 'warnings': num_warnings, 
                            'uploading': num_uploading, 'skipped': num_skipped, 'uploaded': num_uploaded}
                    df = df.append(data, ignore_index=True)
                # reset the data for the new file
                file_path = line.split(',')[3]
                data = {'dir': None, 'path': None, 'warnings': None, 'uploading': None, 'skipped': None, 'uploaded': None}
                num_skipped = num_uploaded = num_uploading = num_warnings = 0
            # or come to the end of the log
            if 'Upload completed in' in line:
                # save the statistics for the last file if they exists
                data = {'dir': os.path.split(file_path)[-1], 'path': file_path, 'warnings': num_warnings, 
                        'uploading': num_uploading, 'skipped': num_skipped, 'uploaded': num_uploaded}
                df = df.append(data, ignore_index=True)
            # identify the number of Warnings thrown
            if 'Warning:' in line:
                num_warnings = num_warnings + 1 
            if 'images with valid mapillary tags' in line:
                # identify the number Uploading 
                num_uploading = int(line.split(' ')[1])
                # identify the number skipped
                num_skipped = int(line.split(' ')[-1].split(')')[0]) 
            # identify the number of files uploaded
            if 'Done uploading' in line:
                num_uploaded = int(line.split(' ')[2])                
    # save the dataframe as a CSV in the given folder
    path_less_ext = os.path.splitext(path)[0]
    csv_path = os.path.join(path_less_ext + '-log_summary.csv')
    with open(csv_path, 'w') as outfile:
        # forcing a \n prevents blank lines in Windows
        df.to_csv(outfile, line_terminator='\n') 
    if args.verbose:
        print(df)
        
if __name__ == '__main__':
    main()
