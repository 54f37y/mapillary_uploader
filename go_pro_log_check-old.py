import os
import argparse
import pandas as pd
from datetime import datetime as dt

def main():
    # Setup argparse
    parser = argparse.ArgumentParser(description="Checks the generated log files and print statistics.")
    parser.add_argument("go_pro_log_path",
                        help="Path to a folder of the go_pro_uploader.py logs")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    args = parser.parse_args()
    if args.verbose:
        print("args: ", args)
    # check the inputs
    path = args.go_pro_log_path
    if not os.path.exists(path):
        print("Error: Given Go Log Pro path does not exist")
        return
    # create a data frame for all the details
    df = pd.DataFrame(columns=['file', 'path', 'warnings', 'uploading', 'skipped', 'uploaded'])
    # work through each file
    for file_name in os.listdir(path):
        file_path = os.path.join(path, file_name)
        if args.verbose:
            print('Checking: {}'.format(file_path))
        # reset the data for each new file
        data = {'file': file_name, 'path': file_path, 'warnings': None, 'uploading': None, 'skipped': None, 'uploaded': None}
        num_skipped = num_uploaded = num_uploading = num_warnings = 0
        if file_name.find(".txt") != -1:
            with open(file_path, 'r') as f:
                for line in f:
                    # identify the number of Warnings thrown
                    if 'Warning:' in line:
                        num_warnings = num_warnings + 1 
                    if 'images with valid mapillary tags' in line:
                        # identify the number Uploading 
                        num_uploading = int(line.split(' ')[1])
                        # identify the number skipped
                        num_skipped = int(line.split(' ')[-1].split(')')[0]) 
                    # identify the number of files uploaded
                    if 'Done uploading' in line:
                        num_uploaded = int(line.split(' ')[2])
            data = {'file': file_name, 'path': file_path, 'warnings': num_warnings, 
                    'uploading': num_uploading, 'skipped': num_skipped, 'uploaded': num_uploaded}
        df = df.append(data, ignore_index=True)
    # save the dataframe as a CSV in the given folder
    today = dt.strftime(dt.today(),'%Y_%m_%d-%H_%M_%S')
    csv_path = os.path.join(path, 'log_summary-' + today + '.csv')
    with open(csv_path, 'w') as outfile:
        # forcing a \n prevents blank lines in Windows
        df.to_csv(outfile, line_terminator='\n') 
    if args.verbose:
        print(df)
        
if __name__ == '__main__':
    main()
