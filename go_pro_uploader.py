from posixpath import split
from sys import version
import uploader
import update_time
import argparse
import os
import time
import logging

def main():
    # Setup argparse
    parser = argparse.ArgumentParser(description="Uploads all images on a Go \
        Pro formatted SD card to Mapillary.")
    parser.add_argument("go_pro_path",
                        help="Path to the DCIM folder of the Go Pro Video \
                        Card")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    parser.add_argument("--offset_angle",
                        default=0,
                        help="Set offset camera angle (90 for right facing, 180 for rear facing, -90 for left facing)")
    parser.add_argument("--offset_secs",
                        default=0,
                        help="Increase the go_pro capture date by x seconds")
    parser.add_argument("--dry_run",
                        action="store_true",
                        help="Process but do not upload images")
    args = parser.parse_args()
    # start timing our operation
    tic = time.perf_counter()
    # print the inputs if we are verbose
    if args.verbose:
        print("args: ", args)
    # check the inputs
    if not os.path.exists(args.go_pro_path):
        print("Error: Given Go Pro path does not exist")
        return
    # Setup logging - note that each run generates a new log file
    timestr = time.strftime("%Y%m%d_%H%M%S")
    logfile = os.path.join('logs', timestr + '-go_pro_upload.log')
    logging.basicConfig(filename=logfile, format='%(asctime)s; %(levelname)s; %(message)s',
                        encoding='utf-8', level=logging.DEBUG, datefmt='%Y-%m-%dT%H:%M:%S%z')
    logging.info('Given args: %s', args)
    # authenticate
    uploader.authenticate(verbose=args.verbose)
    # work through each folder of images
    for dir in os.listdir(args.go_pro_path):
        # we identify the image folders by the presence of GOPRO in their names
        image_path = os.path.join(args.go_pro_path, dir)
        if os.path.isdir(image_path) and dir.find("GOPRO") != -1:
            # we update the exif timestamps of the image files as required
            if args.offset_secs:
                for file in os.listdir(image_path):
                    if file.find(".JPG") != -1:
                        file_path = os.path.join(image_path, file)
                        update_time.update_gopro_date(file_path, args.offset_secs, verbose=False)
                if args.verbose:
                    print('Updated timestamps in %s by %s seconds'.format(str(image_path), str(args.offset_secs)))
                logging.info('Updated timestamps in %s by %s seconds'.format(str(image_path), str(args.offset_secs)))
            # then we upload the whole folder
            uploader.process_and_upload(image_path, camera=dir, verbose=args.verbose, offset_angle=args.offset_angle, dry_run=args.dry_run)
    # stop timing
    toc = time.perf_counter()
    if args.verbose:
        print(f"Upload completed in {toc - tic:0.4f} seconds")
    logging.info(f'Upload completed in {toc - tic:0.4f} seconds')


if __name__ == '__main__':
    main()
