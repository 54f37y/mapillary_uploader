import downloader
import csv_converter
import uploader
import argparse
from os import path, mkdir
from shutil import rmtree
from decouple import config

def main():
    # Setup argparse
    parser = argparse.ArgumentParser(description="Locally downloads, geotags \
        then uploads a CSV of Safe7y image files. Defaults to running a test.")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="Increase output verbosity.")
    parser.add_argument("--dry_run",
                        action="store_true",
                        help="Process but do not upload images")
    parser.add_argument("safe7y_csv_path",
                        help="The Safe7y CSV file that contains the \
                        images for uploading")
    args = parser.parse_args()
    if args.verbose:
        print("args: ", args)
    safety_csv_path = args.safe7y_csv_path
    data_path = config('data_path')
    mapillary_csv_path = path.join(data_path, "mapillary.csv")
    image_dir = path.join(data_path, "images")
    # Create the data folder if it doesn't exist
    if not path.exists(data_path):
        mkdir(data_path)
    # Make sure we are authenticated 
    uploader.authenticate(verbose=args.verbose)
    # Put the safety csv in mapillary format
    images, survey = csv_converter.convert(safety_csv_path, mapillary_csv_path,
                                   verbose=args.verbose)
    # Upload each camera seperately so that they form neat sequences 
    for camera in images.keys():
        # Download the images we need to work on
        downloader.download_s3_dir("safe7y-processed-data", survey,
                                image_dir, images[camera], verbose=args.verbose)
        # Updated the images with their locations
        uploader.update_exif(image_dir, mapillary_csv_path, camera,
                            verbose=args.verbose, dry_run=args.dry_run)
        # Add the necessafery Mapillary fields and upload
        uploader.process_and_upload(image_dir, camera, verbose=args.verbose, 
                                    dry_run=args.dry_run)
        # Clear out the image folder before the next batch
        rmtree(image_dir)

    # TODO: Batch download based on file size

if __name__ == '__main__':
    main()
