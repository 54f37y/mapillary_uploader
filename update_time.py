from uploader import update_exif
import piexif
from datetime import datetime as dt
from datetime import timedelta as td


def main():
    # test on a local file
    directory_path = "C:\\Users\\aiden\\Downloads\\DCIM\\100GOPRO\\G0012933.JPG"
    offset_secs = 175935600
    update_gopro_date(directory_path, offset_secs, True)

def update_gopro_date(path, offset_secs=0, verbose=False):
    # increments the date fields of go pro image exif's by a number of seconds 
    tags_to_update = ['DateTimeOriginal', 'DateTime', 'DateTimeDigitized']
    exif_dict = piexif.load(path)

    if verbose:
        print('\n----------- The original exif -------------')

    # optional print statement to quickly take a look at the file.
    if verbose:
        for ifd in ("0th", "Exif", "GPS", "1st"):
            for tag in exif_dict[ifd]:
                print(piexif.TAGS[ifd][tag]["name"], exif_dict[ifd][tag])
    
    if verbose:
        print('\n----------- Modifying exif fields ------------')

    # modify the exif dict  
    for ifd in ("0th", "Exif", "GPS", "1st"):
        for tag in exif_dict[ifd]:
            if piexif.TAGS[ifd][tag]["name"] in tags_to_update:
                if verbose:
                    print(piexif.TAGS[ifd][tag]["name"], exif_dict[ifd][tag])
                date_str = exif_dict[ifd][tag].decode("utf-8")
                date_dt = dt.strptime(date_str, '%Y:%m:%d %H:%M:%S')
                date_dt = date_dt + td(seconds=int(offset_secs))
                date_str = date_dt.strftime('%Y:%m:%d %H:%M:%S')
                exif_dict[ifd][tag] = date_str.encode("utf-8")
                if verbose:
                    print(piexif.TAGS[ifd][tag]["name"], exif_dict[ifd][tag])

    # replace the existing exif
    piexif.insert(piexif.dump(exif_dict), path)

    if verbose:
        print('\n--------- Exif updated ---------')

if __name__ == '__main__':
    main()
