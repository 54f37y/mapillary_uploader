# Upload Journal

## Setting up the download / Upload

I'm testing this out with buffalo a t2.micro instance accessed through
WSL

I added a 1000GB volume to it and made that the data location

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html

I cleaned up the csv in Excel before I sent it over (made sure the image 
file name is right). And then I sent it using:

`rsync -azP /mnt/c/Users/aiden/Downloads/NV_demo-interstate-mapillary.csv buffalo:/data/`

A really handy command

I'm running the uploader in a seperate screen so that it stays active

`screen -R` to reconnect

## Started demos:

- CA-demo_I80-mapillary.csv

## Finished demos:

- NV_demo-interstate-mapillary.csv
    + All of these got uploaded, but all of the different camera angles are lumped together. I need to seperate them out.

## To Do:
- [ ] Delete that volume when you are done!!! (these are expensive)