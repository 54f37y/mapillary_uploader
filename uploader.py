import subprocess
import os
from decouple import config
import time
import logging

# Upload images described in a CSV file to Mapillary.
def update_exif(images_dir, csv_path, camera, verbose=False, dry_run=False):
    # process the CSV to add the info to the images EXIF
    cmd = ["mapillary_tools", "process_csv", images_dir,
           "--csv_path", csv_path, "--filename_column", "1",
           "--timestamp_column", "2", "--latitude_column",  "3",
           "--longitude_column", "4", "--heading_column", "5", 
            "--verbose"]
    if verbose:
        print('\nApplying CSV data to image: %s\n' % (cmd))
    # run the command, storing the details in a log file for that camera
    with open('logs/' + camera + '-update_exif-log.txt', 'w') as log:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)            
        out,err = proc.communicate()
        log.write(out.decode('utf-8'))
        log.write('\n\n----------------- STDERR -----------------\n\n')
        log.write(err.decode('utf-8'))


def authenticate(verbose=False):
    # Authenticate with mapillary
    cmd = ["mapillary_tools", "authenticate", "--user_name",
           config('mapillary_user_name'), "--user_email",
           config('mapillary_user_email'), "--user_password",
           config('mapillary_user_password')]
    if verbose:
        print('\nAuthenticating: %s\n' % (cmd))
    proc = subprocess.Popen(cmd, 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE, 
                            stderr=subprocess.PIPE)            
    # If we are re-authenticating the process can hang because it is waiting
    # for a confirmatory 'y' - so we just enter it here
    # TODO: there are warnings in the docs about stdin.write blocking, check this
    proc.stdin.write(b'y\n')
    out,err = proc.communicate()
    logging.info(out.decode('utf-8'))
    logging.info(err.decode('utf-8'))

def process_and_upload(images_dir, camera='', offset_angle=0, offset_time=0, verbose=False, dry_run=False, use_gps_time=False):
    # Process and upload images with the proper EXIF location data, like GO PRO
    # Store the metadata in the log file
    timestr = time.strftime("%Y%m%d_%H%M%S")
    json_path = os.path.join('logs', timestr + '-mapillary_image_description.json')
    cmd = ["mapillary_tools",  "process_and_upload", images_dir, 
           "--desc_path", json_path, 
           "--user_name", config('mapillary_user_name'),
           "--organization_username", config('mapillary_organization_user_name'),
           "--organization_key", config('mapillary_organization_key')]
    if dry_run:
        cmd.extend(["--dry_run"])
    if offset_angle:
        cmd.extend(["--offset_angle", offset_angle, "--overwrite_EXIF_direction_tag"])
    if offset_time:
        cmd.extend(["--offset_time", offset_time, "--overwrite_EXIF_time_tag"])
    if use_gps_time:
        cmd.extend(["--use_gps_start_time"])
    if verbose:
        # Print the original command
        print('\nUploading: %s\n' % (cmd))
    logging.info('Uploading: %s' % (cmd))
    # run the process, saving the output in the log file
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, 
                            stderr=subprocess.PIPE)
    out,err = proc.communicate()
    logging.info(out.decode('utf-8'))
    logging.info(err.decode('utf-8'))